﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;


public class Bobby : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    private float moveInput;
 

    private Rigidbody2D rb;
    Animator anim;
    float dirX;


    private bool facingRight = true;
    Vector2 bulletPos;
    public float fireRate = 0.5f;
    float nextFire = 0.0f;

    [SerializeField]
    GameObject codePanel, closedSafe, openedSafe;

    public static bool isSafeOpened = false;




    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        codePanel.SetActive(false);
        closedSafe.SetActive(true);
        openedSafe.SetActive(false);
    
    }

    private void FixedUpdate()
    {

        moveInput = Input.GetAxis("Horizontal2");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }


    }

    private void Update()
    {
        dirX = Input.GetAxisRaw("Horizontal2") * speed * Time.deltaTime;

        transform.position = new Vector2(transform.position.x + dirX, transform.position.y);

        //if (!Settings.status) return;


        if (Input.GetButtonDown("Jump2") && rb.velocity.y == 0)
            rb.AddForce(Vector2.up * jumpForce);
       

        if (Input.GetButtonDown("FireH"))
        {
            
        }

        if (isSafeOpened)
        {
            codePanel.SetActive(false);
            closedSafe.SetActive(false);
  
            openedSafe.SetActive(true);
           
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Equals ("Safe") && !isSafeOpened)
        {
            codePanel.SetActive(true);
            //Settings.status = false;
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name.Equals ("Safe"))
        {
            codePanel.SetActive(false);
        }

    }

  

}














