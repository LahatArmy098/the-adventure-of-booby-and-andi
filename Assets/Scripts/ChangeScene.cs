﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public void MainMenu()
    {
        Application.LoadLevel("MainMenu");
    }
    public void BackMainMenu()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Play()
    {
        Application.LoadLevel("SelectLevel");
    }

    public void PlayEasy()
    {
        Application.LoadLevel("EasyStage1");
    }
    public void PraStage1()
    {
        Application.LoadLevel("AfterStage1");
    }
    public void PlayStage2()
    {
        Application.LoadLevel("EasyStage2");
    }

    public void PraStage2()
    {
        Application.LoadLevel("AfterStage2");
    }
    public void PlayStage3Time()
    {
        Application.LoadLevel("EasyStage3");
    }

    public void PlayStage3Speed()
    {
        Application.LoadLevel("EasyStage4");
    }

    public void Win()
    {
        Application.LoadLevel("Win");
    }

    public void Credit()
    {
        Application.LoadLevel("Credit");
    }
    public void Command()
    {
        Application.LoadLevel("Command");
    }

    public void Exit()
    {
        Application.Quit();
    }
   
}
