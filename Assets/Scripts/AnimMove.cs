﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimMove : MonoBehaviour {

    float dirX, moveSpeed;
    public GameObject Peluru;
    public Transform bulletSpawn;
    private Rigidbody2D rb;

    Animator anim;

	void Start () {
        anim = GetComponent<Animator>();
        moveSpeed = 10f;
        rb = GetComponent<Rigidbody2D>();
    }
	

	void Update () {
        dirX = Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime;

        transform.position = new Vector2(transform.position.x + dirX, transform.position.y);

        if (dirX != 0 && !anim.GetCurrentAnimatorStateInfo(0).IsName("HackAnim"))
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        if (Input.GetButtonDown ("Fire1") && !anim.GetCurrentAnimatorStateInfo(0).IsName("HackAnim"))
        {
            anim.SetBool("isWalking", false);
            anim.SetTrigger("hit");
            Tembak();
        }
    }

    void Tembak()
    {
        GameObject b = (GameObject)(Instantiate(Peluru, bulletSpawn.position, bulletSpawn.rotation));

        b.GetComponent<Rigidbody2D>().AddForce(transform.up * 1000);



        Destroy(b, 2);
    }
}
