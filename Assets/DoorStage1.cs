﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorStage1 : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {

            Application.LoadLevel("AfterStage1");

        }
    }
}
