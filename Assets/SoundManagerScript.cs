﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

    public static AudioClip fireSound, enemyDogSound, enemyBlizardSound;
    static AudioSource audioSrc;

	// Use this for initialization
	void Start () {

        fireSound = Resources.Load<AudioClip>("Bat sound fx");
        enemyDogSound = Resources.Load<AudioClip>("dog fx");
        enemyBlizardSound = Resources.Load<AudioClip>("lizard fx");

        audioSrc = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "Bat sound fx":
                audioSrc.PlayOneShot(fireSound);
                break;
            case "dog fx":
                audioSrc.PlayOneShot(enemyDogSound);
                break;
            case "lizard fx":
                audioSrc.PlayOneShot(enemyBlizardSound);
                break;
        }
    }
}
