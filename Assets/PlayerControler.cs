﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{

    public float speed;
    public float jumpForce;
    private float moveInput;
    public GameObject Peluru;
    public Transform bulletSpawn;
    public Transform bulletSpawn2;
    private AudioSource suara;

    private Rigidbody2D rb;
    Animator anim;
    float dirX;


    private bool facingRight = true;
    Vector2 bulletPos;
    public float fireRate = 0.5f;
    float nextFire = 0.0f;



    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        suara = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {

        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if(facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if(facingRight == true && moveInput < 0)
        {
            Flip();
        }

      
    }

    private void Update()
    {
        // (!Settings.status) return;
        if (Input.GetKey("p"))
        {
            GameObject PauseBtn = GameObject.Find("GameManagerandAudio");
            GameManagerScript PauseGame = PauseBtn.GetComponent<GameManagerScript>();
            PauseGame.PauseGame();
            
        }

        dirX = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;

        transform.position = new Vector2(transform.position.x + dirX, transform.position.y);

        if (dirX != 0 && !anim.GetCurrentAnimatorStateInfo(0).IsName("HackAnim"))
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        if (Input.GetButtonDown("Jump1") && rb.velocity.y == 0)
            rb.AddForce(Vector2.up * jumpForce);

        if (Input.GetButtonDown("Fire1") && !anim.GetCurrentAnimatorStateInfo(0).IsName("HackAnim") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            anim.SetBool("isWalking", false);
            anim.SetTrigger("hit");
            fire();
            suara.Play();
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    void fire()
    {


        if (facingRight)
        {
            GameObject b1 = (GameObject)(Instantiate(Peluru, bulletSpawn.position, bulletSpawn.rotation));
            b1.GetComponent<Rigidbody2D>().AddForce(transform.right * 4000);

            Destroy(b1, 1);
        }
        else
        {
            GameObject b2 = (GameObject)(Instantiate(Peluru, bulletSpawn.position, bulletSpawn.rotation));
            b2.GetComponent<Rigidbody2D>().AddForce(transform.right * -4000);

            Destroy(b2, 1);
        }

        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name.Equals("Enemy1"))
        {
            SceneManager.LoadScene("Win");
        }
    }
}