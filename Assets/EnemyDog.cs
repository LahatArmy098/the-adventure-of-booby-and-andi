﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDog : MonoBehaviour {

    public GameObject blood;

    public float moveSpeed = 3f;
    Transform leftWayPoint, rightWayPoint;
    Vector3 localScale;
    bool movingRight = true;
    Rigidbody2D rb;
        
	// Use this for initialization
	void Start () {

        localScale = transform.localScale;
        rb = GetComponent<Rigidbody2D>();
        leftWayPoint = GameObject.Find("LeftWayPoint").GetComponent<Transform>();
        rightWayPoint = GameObject.Find("RightWayPoint").GetComponent<Transform>();

    }
	
	// Update is called once per frame
	void Update () {

        //if (!Settings.status) return;

        if (transform.position.x > rightWayPoint.position.x)
            movingRight = false;
        if (transform.position.x < leftWayPoint.position.x)
            movingRight = true;

        if (movingRight)
            moveRight();
        else
            moveLeft();
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wave")
        {
            Destroy(collision.gameObject);

            Destroy(gameObject);
        }

        if (collision.gameObject.tag.Equals("Player"))
        {

            Application.LoadLevel("Lose");

        }

        if (collision.gameObject.tag.Equals("Respawn"))
        {

            Application.LoadLevel("Lose");

        }
    }

    void moveRight()
    {
        movingRight = true;
        localScale.x = 5;
        transform.localScale = localScale;
        rb.velocity = new Vector2(localScale.x * moveSpeed, rb.velocity.y);
    }

    void moveLeft()
    {
        movingRight = false;
        localScale.x = -5;
        transform.localScale = localScale;
        rb.velocity = new Vector2(localScale.x * moveSpeed, rb.velocity.y);
    }


}
